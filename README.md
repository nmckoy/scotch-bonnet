# SCOTCH BONNET

## Running locally
To run, cd into app directory and use:
```
npm run start
```

## Building for Deployment
for building for deployment cd to app directory and use:
```
STAGE={{stage}} npm run build
```

## Deploying to Squarespace
cd into the stage directory you would like to deploy and run:
```
STAGE={{stage}} ../app/push.sh
```
value of STAGES env var should only be either `staging` or `prod`

after deployment verify using these URLs
```
staging = https://jacy-cunningham-373w.squarespace.com
prod    = https://www.thejacymethod.com
```
