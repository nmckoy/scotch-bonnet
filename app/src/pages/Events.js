import React, { Component } from 'react'
import { Grid, Row, Col } from 'react-flexbox-grid'

import Footer from '../components/Footer'
import { removeActiveHome } from '../Utils'

const text_content_lead = {
  minHeight: '500px',
  color: 'rgb(35, 35, 35)',
  backgroundColor: 'white',
  textAlign: 'center',
  padding: '25px',
  textAlign: 'justify'
}
const text_content_lead_header = {
    borderBottom: '1px solid #b5b5b5'
}

const content_start_style = {
  fontWeight: '600',
  fontSize: '40px'
}
const content_sub_style = {
  paddingTop: '10px',
  fontSize: '16px',
  textAlign: 'center'
}
const title_row_style = {
    padding: '7px 7px 6px 7px'
}
const title_col_style = {
  background: '#1F1C2C'

}
const title_content_style = {
    paddingBottom: '7px',
    paddingLeft: '7px',
    color: 'white',
    fontWeight: '600'
}

class Events extends Component {
  componentDidMount() {
    // hack to clean up the active class on home nav link
    // for some reason it is always active when link_to = /
    removeActiveHome()
    
    const script = document.createElement("script");
    const magnific_code = `
      $('#play-vid')
        .magnificPopup({
            items: [{
              src: 'https://vimeo.com/220839749'
            }],
            type:'iframe'
          });`;
        
    script.type = 'text/javascript';
    script.text = magnific_code;

    document.body.appendChild(script);
  }

  render() {
    return  <div>
              <div className="events-bgimg-1">
          			<div className="caption">
          				<span className="border"></span>
          			</div>
          		</div>
          		<div style={ text_content_lead }>

                {/* WUNDERLUST SQUAW VALLEY */}
        		    <Grid>
                    <Row style={ title_row_style }>
                      <Col xs>
                        <h1 style={ content_start_style }>Wanderlust Squaw Valley</h1>
        		          </Col>
        		        </Row>
        		        <Row style={ title_row_style }>
        		          <Col className='events-image-2'>
        		          </Col>
        		        </Row>
                    <Row>
                      <Col xs={12}>
                        <Row center="xs" style={ Object.assign({}, content_sub_style, title_row_style ) }>
                          <Col xs={12} sm={6} md={6} lg={6}>
                            <p>July 19-22, 2018</p>
                            <p>North Lake Tahoe, CA</p>
                            <p><a href="https://wanderlust.com/festivals/squaw-valley/" target="_blank">https://wanderlust.com/festivals/squaw-valley/</a></p>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
        		    </Grid>

                {/* WUNDERLUST WELLSPRING */}
        		    <Grid>
                    <Row style={ title_row_style }>
                      <Col xs>
                        <h1 style={ content_start_style }>Wanderlust Wellspring</h1>
        		          </Col>
        		        </Row>
        		        <Row style={ title_row_style }>
        		          <Col className='events-image-3'>
        		          </Col>
        		        </Row>
                    <Row>
                      <Col xs={12}>
                        <Row center="xs" style={ Object.assign({}, content_sub_style, title_row_style ) }>
                          <Col xs={12} sm={6} md={6} lg={6}>
                            <p>October 26-28, 2018</p>
                            <p>Palm Springs, CA </p>
                            <p><a href="https://wanderlust.com/wellspring/" target="_blank">https://wanderlust.com/wellspring/</a></p>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
        		    </Grid>

        		    <Grid>
                    <Row style={ title_row_style }>
                      <Col xs>
                        <h1 style={ content_start_style }>Trap Jumpin</h1>
        		          </Col>
        		        </Row>
        		        <Row style={ title_row_style }>
        		          <Col className='events-vid-1'>
                        <div className="col-md-4">
                        </div>
                        <div className="col-md-4">
                        </div>
        		          </Col>
        		        </Row>
                    <Row>
                      <Col xs={12}>
                        <Row center="xs" style={ Object.assign({}, content_sub_style, title_row_style ) }>
                          <Col xs={12} sm={6} md={6} lg={6}>
                            <p>Event schedule coming soon...</p>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
        		    </Grid>
      
        		    {/* example events page block <Grid>
                    <Row style={ title_row_style }>
                      <Col xs>
                        <h1 style={ content_start_style }>Trap Jumpin</h1>
        		          </Col>
        		        </Row>
        		        <Row style={ title_row_style }>
        		          <Col id='play-vid' className='events-vid-1'>
        		            <div className="col-md-4">
                            <i className="fa fa-play-circle" aria-hidden="true"></i>
                        </div>
                        <div className="col-md-4">
                        </div>
                        <div className="col-md-4">
                        </div>
        		          </Col>
        		        </Row>
                    <Row>
                      <Col xs={12}>
                        <Row center="xs" style={ Object.assign({}, content_sub_style, title_row_style ) }>
                          <Col xs={12} sm={6} md={6} lg={6}>
                            <p>Event schedule coming soon...</p>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
        		        </Grid>*/}

              </div>
            </div>;
    }
}

export default Events
