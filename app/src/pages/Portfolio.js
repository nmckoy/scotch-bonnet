import React, { Component } from 'react'
import { Grid, Row, Col } from 'react-flexbox-grid'
import Gallery from 'react-grid-gallery'

import Footer from '../components/Footer'
import Company from '../components/Company'
import { removeActiveHome, jacy_images, jacy_images2, client_images, partner_images } from '../Utils'
// import Gallery from '../components/Gallery'

const text_content_lead = {
  minHeight: '550px',
  color: 'rgb(35, 35, 35)',
  backgroundColor: 'white',
  textAlign: 'center',
  padding: '25px',
  textAlign: 'justify'
}
const text_content_lead_header = {
    borderBottom: '1px solid #b5b5b5'
}
const row_style = {
    padding: '7px 9px 0px 8px'
}
const title_row_style = {
    padding: '7px 7px 6px 7px'
}
const image_row_style = {
  marginTop: '7px'
}
const title_col_style = {
    background: '#1F1C2C'
}
const title_content_style = {
    paddingBottom: '7px',
    paddingLeft: '7px',
    color: 'white',
    fontWeight: '600'
}


// http://unitegallery.net/
// use this for gallery
class Portfolio extends Component {
    componentDidMount() {
        // hack to clean up the active class on home nav link
        // for some reason it is always active when link_to = /
        removeActiveHome()
        
        // setTimeout(() => { 
        //     let tiles = document.getElementsByClassName('tile')
        //     for (let i = tiles.length-1; i >= 0; i--) {
        //         tiles[i].setAttribute('style', tiles[i].getAttribute('style') + 'max-width: 500px;')
        //     }
        // }, 1)
        
        const script = document.createElement("script");
        const magnific_code = `
          $('#play-vid1')
            .magnificPopup({
                items: [{
                  src: 'https://www.youtube.com/watch?v=TVlD3Elr79c'
                }],
                type:'iframe'
              });
          $('#play-vid2')
            .magnificPopup({
                items: [{
                  src: 'https://vimeo.com/157372560'
                }],
                type:'iframe'
              });
          $('#play-vid3')
            .magnificPopup({
                items: [{
                  src: 'https://vimeo.com/221906910'
                }],
                type:'iframe'
              });
          $('#play-vid4')
            .magnificPopup({
                items: [{
                  src: 'https://vimeo.com/203504047'
                }],
                type:'iframe'
              });
          $('#play-vid5')
            .magnificPopup({
                items: [{
                  src: 'https://vimeo.com/191594817'
                }],
                type:'iframe'
              });`;
            
        script.type = 'text/javascript';
        script.text = magnific_code;
    
        document.body.appendChild(script);
    }
    
    render() {
        
        return  <div>
                  <div className="portfolio-bgimg-1">
              			<div className="caption">
              				<span className="border"></span>
              			</div>
              		</div>
              
              		<div style={ text_content_lead }>
              		    <Grid>
              		        <Row style={ row_style }>
              		            <Col xs style={ title_col_style }>
              		                <h3 style= { title_content_style }>Portfolio</h3>
              		            </Col>
              		        </Row>
              		        <Row style={ image_row_style }>
              		            <Col xs>
              		                <Gallery images={jacy_images} >
              		                </Gallery>
              		            </Col>
              		        </Row>
              		        <Row style={row_style}>
              		            <Col xs id='play-vid1' className='portfolio-vid-1'>
              		                <div className="col-md-4">
                                        <i className="fa fa-play-circle" aria-hidden="true"></i>
                                    </div>
                                    <div className="col-md-4">
                                    </div>
                                    <div className="col-md-4">
                                    </div>
              		            </Col>
              		        </Row>
              		        <Row style={row_style}>
              		            <Col xs={12} sm={8} md={8} lg={8} id='play-vid2' className='portfolio-vid-2'>
              		                <div className="col-md-4">
                                        <i className="fa fa-play-circle" aria-hidden="true"></i>
                                    </div>
                                    <div className="col-md-4">
                                    </div>
                                    <div className="col-md-4">
                                    </div>
              		            </Col>
              		            <Col xs={12} sm={4} md={4} lg={4} id='play-vid3' className='portfolio-vid-3'>
              		                <div className="col-md-4">
                                        <i className="fa fa-play-circle" aria-hidden="true"></i>
                                    </div>
                                    <div className="col-md-4">
                                    </div>
                                    <div className="col-md-4">
                                    </div>
              		            </Col>
              		        </Row>
              		        <Row style={ image_row_style }>
              		            <Col xs>
              		                <Gallery images={jacy_images2} >
              		                </Gallery>
              		            </Col>
              		        </Row>
              		        <Row style={row_style}>
              		            <Col xs={12} sm={8} md={8} lg={8} id='play-vid4' className='portfolio-vid-4'>
              		                <div className="col-md-4">
                                        <i className="fa fa-play-circle" aria-hidden="true"></i>
                                    </div>
                                    <div className="col-md-4">
                                    </div>
                                    <div className="col-md-4">
                                    </div>
              		            </Col>
              		            <Col xs={12} sm={4} md={4} lg={4} id='play-vid5' className='portfolio-vid-5'>
              		                <div className="col-md-4">
                                        <i className="fa fa-play-circle" aria-hidden="true"></i>
                                    </div>
                                    <div className="col-md-4">
                                    </div>
                                    <div className="col-md-4">
                                    </div>
              		            </Col>
              		        </Row>
              		        {/* Client pics */}
              		        <Row style={ title_row_style }>
              		            <Col xs style={ title_col_style }>
              		                <h3 style={ title_content_style }>Clients</h3>
              		            </Col>
              		        </Row>
              		        <Row>
              		          <Col xs={12}>
              		            <Row center="xs" style={row_style}>
                  		            <Company href='http://www.nike.com/' cssId='portfolio-nike-img' />
                  		            <Company href='http://lifeisgood.com/' cssId='portfolio-lifeisgood-img' />
                  		            <Company href='https://www.jaybirdsport.com/en-us' cssId='portfolio-jaybird-img' />
                  		            <Company href='http://www.summit.co/' cssId='portfolio-summit-img' />
                  		        </Row>
                  		        <Row center="xs" >
                  		            <Company href='https://lokai.com/store/black-white' cssId='portfolio-lokai-img' />
                  		            <Company href='http://www.griffinloop.com/' cssId='portfolio-griffinloop-img' />
                  		            <Company href='https://www.paradigmagency.com/' cssId='portfolio-paradigm-img' />
                  		            <Company href='http://www.goodstuffeatery.com/' cssId='portfolio-goodstuffeatery-img' />
                  		        </Row>
                  		        <Row center="xs" >
                  		            <Company href='http://www.qoya.love/' cssId='portfolio-qoya-img' />
                  		            <Company href='http://www.mikeposner.com/' cssId='portfolio-mikeposner-img' />
                  		            <Company href='http://starwoodcapital.com/' cssId='portfolio-starwood-img' />
                  		            <Company href='http://pilatesplatinum.com/' cssId='portfolio-pilatesplatinum-img' />
                  		        </Row>
                  		        <Row center="xs" >
                  		            <Company href='https://www.wolfdiningla.com/' cssId='portfolio-wolfdinigla-img' />
                  		            <Company href='http://mick.co/' cssId='portfolio-mick-img' />
                  		            <Company href='http://www.indais.com/' cssId='portfolio-indais-img' />
                  		            <Company href='https://handleonlywithlove.com/' cssId='portfolio-howl-img' />
                  		        </Row>
                  		        <Row center="xs" >
                  		            <Company href='http://bamcommunications.biz/ ' cssId='portfolio-bam-img' />
                  		            <Company href='https://www.taskus.com/' cssId='portfolio-taskus-img' />
                  		            <Company href='http://humbleriot.com/' cssId='portfolio-humbleriot-img' />
                  		            <Company href='https://fueled.com/coworking-space-nyc/#welcome' cssId='portfolio-fueled-img' />
                  		        </Row>
              		          </Col>
              		        </Row>
              		        
              		        {/* Partner pics */}
              		        <Row style={ title_row_style }>
              		            <Col xs style={ title_col_style }>
              		                <h3 style={ title_content_style }>Partners</h3>
              		            </Col>
              		        </Row>
              		        <Row style={row_style}>
        <Company href='http://trapjumpin.com/' cssId='portfolio-trapjumpin-img' xsSize={12} smSize={12} mdSize={12}/>
              		        </Row>
              		    </Grid>
              		</div>
              		
              	</div>
    } 
}

export default Portfolio
