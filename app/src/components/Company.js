import React, { Component } from 'react'
import { Col } from 'react-flexbox-grid'

class Company extends Component {
    openWindow(param) {
        window.open(this.props.href, '_blank')
    }
    
    constructor(props) {
        super(props)
        
        this.openWindow = this.openWindow.bind(this)
    }
    
    render() {
        return (
            <Col 
                xs={this.props.xsSize ? this.props.xsSize : 5}
                sm={this.props.smSize ? this.props.smSize : 5}
                md={this.props.mdSize ? this.props.mdSize : 5}
                lg
                onClick={ this.openWindow } 
                id={ this.props.cssId }
            >
            </Col>
        )
    }
}

export default Company;