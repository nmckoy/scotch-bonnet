export const removeActiveHome = () => {
    let pathname = window.location.pathname
        if (pathname != '/') {
            document.getElementById('home-link').parentElement.removeAttribute('class')
        } else {
            document.getElementById('home-link').parentElement.setAttribute('class', 'active')
        }
}

export const jacy_images =
    [{
            src: "https://i.imgur.com/NLPmheD.jpg",
            thumbnail: "https://i.imgur.com/NLPmheD.jpg",
            thumbnailWidth: 300,
            thumbnailHeight: 230
    },
    {
            src: "https://i.imgur.com/K64NDEu.jpg",
            thumbnail: "https://i.imgur.com/K64NDEu.jpg",
            thumbnailWidth: 520,
            thumbnailHeight: 350
    },
    {
            src: "http://i.imgur.com/QCjArCS.jpg",
            thumbnail: "http://i.imgur.com/QCjArCS.jpg",
            thumbnailWidth: 230,
            thumbnailHeight: 150
    },
    {
            src: "https://i.imgur.com/Ljb9IRJ.jpg",
            thumbnail: "https://i.imgur.com/Ljb9IRJ.jpg",
            thumbnailWidth: 330,
            thumbnailHeight: 212
    },
    {
            src: "https://i.imgur.com/Br44HJv.jpg",
            thumbnail: "https://i.imgur.com/Br44HJv.jpg",
            thumbnailWidth: 320,
            thumbnailHeight: 212
    },
    {
            src: "https://i.imgur.com/KfS19ts.png",
            thumbnail: "https://i.imgur.com/KfS19ts.png",
            thumbnailWidth: 320,
            thumbnailHeight: 250
    },
    {
            src: "https://i.imgur.com/i41nAqF.jpg",
            thumbnail: "https://i.imgur.com/i41nAqF.jpg",
            thumbnailWidth: 300,
            thumbnailHeight: 212
    },
    {
            src: "https://i.imgur.com/nJ5XbuS.png",
            thumbnail: "https://i.imgur.com/nJ5XbuS.png",
            thumbnailWidth: 350,
            thumbnailHeight: 230
    },
    {
            src: "https://i.imgur.com/PzNYmGy.jpg",
            thumbnail: "https://i.imgur.com/PzNYmGy.jpg",
            thumbnailWidth: 360,
            thumbnailHeight: 230
    },
    {
            src: "http://i.imgur.com/Fyob74V.jpg",
            thumbnail: "http://i.imgur.com/Fyob74V.jpg",
            thumbnailWidth: 360,
            thumbnailHeight: 230
    },
    {
            src: "https://i.imgur.com/9Tsiwk5.jpg",
            thumbnail: "https://i.imgur.com/9Tsiwk5.jpg",
            thumbnailWidth: 385,
            thumbnailHeight: 200
    },
    {
            src: "http://i.imgur.com/6jAz73b.jpg",
            thumbnail: "http://i.imgur.com/6jAz73b.jpg",
            thumbnailWidth: 110,
            thumbnailHeight: 150
    }];

export const jacy_images2 =
    [{
            src: "https://i.imgur.com/MBLHFen.jpg",
            thumbnail: "https://i.imgur.com/MBLHFen.jpg",
            thumbnailWidth: 480,
            thumbnailHeight: 470
    },
    {
            src: "https://i.imgur.com/UZOW2wg.jpg",
            thumbnail: "https://i.imgur.com/UZOW2wg.jpg",
            thumbnailWidth: 370,
            thumbnailHeight: 250
    },
    {
            src: "https://i.imgur.com/OPqiQQ1.jpg",
            thumbnail: "https://i.imgur.com/OPqiQQ1.jpg",
            thumbnailWidth: 320,
            thumbnailHeight: 212
    },
    {
            src: "https://i.imgur.com/1lyaEoL.jpg",
            thumbnail: "https://i.imgur.com/1lyaEoL.jpg",
            thumbnailWidth: 350,
            thumbnailHeight: 230
    },
    {
            src: "http://i.imgur.com/VJozwRH.png",
            thumbnail: "http://i.imgur.com/VJozwRH.png",
            thumbnailWidth: 150,
            thumbnailHeight: 150
    },
    {
            src: "http://i.imgur.com/2H6hKZR.jpg",
            thumbnail: "http://i.imgur.com/2H6hKZR.jpg",
            thumbnailWidth: 130,
            thumbnailHeight: 150
    },
    {
            src: "https://i.imgur.com/B5PPwnO.jpg",
            thumbnail: "https://i.imgur.com/B5PPwnO.jpg",
            thumbnailWidth: 260,
            thumbnailHeight: 230
    },
    {
            src: "https://i.imgur.com/u8zkqQm.jpg",
            thumbnail: "https://i.imgur.com/u8zkqQm.jpg",
            thumbnailWidth: 240,
            thumbnailHeight: 230
    },
    {
            src: "https://i.imgur.com/MXoazql.jpg",
            thumbnail: "https://i.imgur.com/MXoazql.jpg",
            thumbnailWidth: 360,
            thumbnailHeight: 230
    },
    {
            src: "https://i.imgur.com/ooBsJ5U.png",
            thumbnail: "https://i.imgur.com/ooBsJ5U.png",
            thumbnailWidth: 260,
            thumbnailHeight: 230
    }]
